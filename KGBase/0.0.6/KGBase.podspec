Pod::Spec.new do |s|

s.name         = "KGBase"
s.version      = "0.0.6"
s.summary      = "Categories for Kogi Development"
s.homepage     = "http://www.kogimobile.com"
s.license      = 'MIT'
s.requires_arc = true
s.platform     = :ios, '7.0'
s.framework  = 'AVFoundation'
s.framework  = 'Security'
s.framework  = 'CoreGraphics'
s.framework  = 'UIKit'
s.framework  = 'ACCelerate'

s.author             = { "Ernesto Carrion" => "ernesto@kogimobile.com" }

s.source       = { :git => "git@bitbucket.org:kogimobilesas/kgbase.git", :tag => "0.0.6" }
s.source_files  = "KGBase/Classes/*"

end